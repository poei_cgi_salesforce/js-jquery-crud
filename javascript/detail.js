function param(name) {
    return (location.search.split(name + '=')[1] || '').split('&')[0];
}
// Je réccupére le paramètre ID dans l'URL
let id = param('id');

// Quand la page est chargé
$( document ).ready(function() {
    // Je fais une requête sur mon serveur pour réccupérer mon voyage en fonction de son id
    $.ajax({
        url: "http://localhost:3000/travels/"+ id,
        method: 'GET'
    }).done(function(data) {
        // Lorsque j'ai une réponse, je vais afficher dans ma page les informations qui proviennent du serveur
        $('#titlePage').text("Voyage vers " +data.destination);
        $('#voyageName').text(data.destination);
        $('#category').text(data.type);
        $('#photo').html('<img src="'+data.photo+'" alt="Photo de '+data.destination+'">');

        // Je masque mon loader et j'affiche mon contenu
        $('#spinner').addClass('d-none');
        $('#main-container').removeClass('d-none');

    }).fail(function() {
        alert( "error" );
    });
});