$('#formAdd').submit(function($event){

    $destination = $('#destination').val();
    $photo = $('#photo').val();
    $categorie = $('#categorie').find(":selected").val();


    $.ajax({
        url: "http://localhost:3000/travels/",
        method: 'POST',
        data: {
            "destination": $destination,
            "photo": $photo,
            "type": $categorie
        }
    }).done(function(data) {
        window.location.href = 'index.html'
    }).error(function(){
        alert('Une erreur s\'est produit !');
    })


    $event.stopImmediatePropagation();
    $event.preventDefault();
});

