function param(name) {
    return (location.search.split(name + '=')[1] || '').split('&')[0];
}
// Je réccupére le paramètre ID dans l'URL
let id = param('id');

$( document ).ready(function() {
    // Je fais une requête sur mon serveur pour réccupérer mon voyage en fonction de son id
    $.ajax({
        url: "http://localhost:3000/travels/"+ id,
        method: 'GET'
    }).done(function(data) {
        // Lorsque j'ai une réponse, je vais afficher dans ma page les informations qui proviennent du serveur
        $('#titlePage').text("Voyage vers " +data.destination);
        $('#h1Page').text("Editer le voyage " +data.destination);
        $('#destination').val(data.destination);
        $('#photo').val(data.photo);
        $('#'+data.type).attr('selected', true);

        // Je masque mon loader et j'affiche mon contenu
        $('#spinner').addClass('d-none');
        $('#main-container').removeClass('d-none');

    }).fail(function() {
        alert( "error" );
    });
});


$('#formUpdate').submit(function($event){



    $destination = $('#destination').val();
    $photo = $('#photo').val();
    $categorie = $('#categorie').find(":selected").val();

    $.ajax({
        url: "http://localhost:3000/travels/"+ id,
        method: 'PUT',
        data: {
            "destination": $destination,
            "photo": $photo,
            "type": $categorie
        }
    }).done(function(data) {
        window.location.href = 'index.html';
    }).error(function(){
        alert('Une erreur s\'est produit !');
    })
    $event.preventDefault();
    $event.stopImmediatePropagation();


});

