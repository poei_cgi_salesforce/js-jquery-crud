// En JQUERY on réccupére notre tableau en fonction de son ID (travelList)
$table = $('#travelList');
// En JQUERY on réccupére le body de notre tableau en fonction de son ID (tableVoyageBody)
$tableBody = $('#tableVoyageBody');

// Cette fonction en JQuery est lancée automatiquement dès que la page est chargée
$(document).ready(function () {
    // Quand la page est chargée on fait une requête sur notre serveur pour réccupérer tous nos voyages
    $.ajax({
        url: "http://localhost:3000/travels",
        method: 'GET'
    }).done(function (data) {
        // Quand le serveur à répondu, on va écrire dans notre tableau pour afficher les lignes en fonction de nos voyage
        $tableBody.html('');
        // Sur chacune de mes lignes, dans la colonnes action je fais un lien qui prend en paramètre un ID (ligne 23)
        data.forEach(elem => {
            $tableBody.append('<tr> \
                <th scope="row">' + elem.id + '</th> \
            <td>' + elem.destination + '</td> \
            <td><img class="img-thumbnail mignature" src="' + elem.photo + '" alt="Photo de ' + elem.destination + '"></td> \
            <td>' + elem.type + '</td> \
            \<td>\
            \<a href="detail.html?id=' + elem.id + '" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="En savoir plus !">\
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">\
                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>\
                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>\
            </svg>\
            </a>\
            <button class="btn btn-danger deleteTravel" data-id="' + elem.id + '">\
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">\
            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>\
            </svg>\
            </button>\
            \
             <a href="update.html?id=' + elem.id + '" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="En savoir plus !">\
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">\
            <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>\
            </svg>\
            </a>\
            </td> \
        </tr>');




        });

        $('.deleteTravel').click(function(){
            let id = $(this).data('id');
            let elemToDelete = $(this).parent().parent();

            $.ajax({
                url: "http://localhost:3000/travels/"+id,
                method: 'DELETE'
            }).done(function() {
                elemToDelete.remove();
            });

        });

        // Je n'affiche plus mon timer
        $('#spinner').addClass('d-none');
        // J'affiche mon tableau
        $('#main-container').removeClass('d-none');

    }).fail(function () {
        alert("error");
    });
});




